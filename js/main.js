var sections = document.querySelectorAll('.section');
var offset = innerHeight * 0.9;
function setAnimation() {
  sections.forEach(function(item) {
    if (item.getBoundingClientRect().top < offset) {
      item.classList.add('animated')
      var timeout = setTimeout(function(){
        item.classList.add('animation_ends')
      },7000)
    }
  })
}

document.addEventListener('scroll', setAnimation)
setAnimation()

function modalControls(e){
  var body = document.querySelector('body');
  if (e.target.classList.contains('modal_btn')) {
    event.preventDefault()
    var modal = document.getElementById(e.target.getAttribute('data-target'));
    modal.classList.add('active');
    body.classList.add('overlay');
  }
  if (e.target.classList.contains('overlay') || e.target.classList.contains('close')) {
    var modal = document.querySelector('.modal.active');
    modal.classList.remove('active');
    body.classList.remove('overlay');
  }
}

document.addEventListener('click', modalControls)

function navControls() {
    var elem = event.target
    var nav = document.querySelector('nav')
    var header = document.querySelector('header')
    if (elem.classList.contains('show_menu')) {
        elem.classList.toggle('active')
        nav.classList.toggle('active')
        header.classList.toggle('active')
    }

    if (document.querySelector('.search_toggler')) {
      if (elem.classList.contains('search_toggler')) {
        document.querySelector('.search_form').classList.toggle('active');
      }
      else {
        var notHide = false;
        event.path.forEach((item)=>{
            if(item == document.querySelector('.search_form')) {
              notHide = true
              return notHide
            }
        })
        if (notHide) {
          return
        }
        else {
          document.querySelector('.search_form').classList.remove('active');

        }
      }
    }
    if (elem.classList.contains('src')){
      nav.classList.remove('active')
      document.querySelector('.show_menu').classList.remove('active')
    }
}
document.addEventListener('click', navControls)


function tabs(e){
  e = event.target
  var currentTabs = e.parentNode.parentNode
  if (e.classList.contains('tab_toggler')) {
    currentTabs.querySelectorAll('.tab_content').forEach((item) => {
      item.classList.remove('active');
    })
    currentTabs.querySelectorAll('.tab_toggler').forEach((item) => {
      item.classList.remove('active');
    })
    e.classList.add('active')
    currentTabs.querySelector(e.getAttribute('data-target')).classList.add('active')
  }
}

document.addEventListener('click', tabs);

$(function($){
  $('[type="tel"]').mask("+7 (999) 999-99-99");
})



var x, i, j, l, ll, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.classList.add('input')
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        a.classList.add('filled');
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
	if($('.contact_us_contacts').hasClass("select-arrow-active_con")){
		$('.contact_us_contacts').removeClass("select-arrow-active_con");
	}else{
		$('.contact_us_contacts').addClass("select-arrow-active_con");
	}
  });
}

function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }

}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);

$(document).on("click", "a[href^='#']", function (e) {
    var id = $(this).attr("href");
    if ($(id).length > 0) {
      e.preventDefault();

      // trigger scroll
      controller.scrollTo(id);

        // if supported by the browser we can even update the URL.
      if (window.history && window.history.pushState) {
        history.pushState("", document.title, id);
      }
    }
  });


var preloader = setTimeout(function(){
  document.querySelector('body').classList.remove('load')
  document.querySelector('.offer').classList.add('animated')
  document.querySelector('.header').classList.add('animated')
  document.querySelector('.preloader').classList.add('hidden')
},4000)


function headerStyler() {
  var sections = document.querySelectorAll('section');
  sections.forEach(function(item){
    if (item.getBoundingClientRect().top <= 0) {
      if (item.id == 'offer') {
        document.querySelector('.header').classList = 'header center';
      }
      else if (item.id == 'equipment') {
        document.querySelector('.header').classList = 'header center';
        document.querySelector('.header').classList.add('header-equipment');
        document.querySelector('#nav_equipment').classList.add('active');
      }

      else if (item.id == 'pyrolysis') {
        document.querySelector('.header').classList = 'header center';
        document.querySelector('.header').classList.add('header-pyrolysis');
        document.querySelector('#nav_technologies').classList.add('active');
      }
      else if (item.id == 'technologies') {
        document.querySelector('.header').classList = 'header center';
        document.querySelector('.header').classList.add('header-technologies');
      }
      else if (item.id == 'plus') {
        document.querySelector('.header').classList = 'header center';
        document.querySelector('.header').classList.add('header-plus');
        document.querySelector('#nav_pluses').classList.add('active');
      }
      else if (item.id == 'about') {
        document.querySelector('.header').classList = 'header center';
        document.querySelector('.header').classList.add('header-about');
        document.querySelector('#nav_about').classList.add('active');
      }
      else if (item.id == 'contact_us') {
        document.querySelector('.header').classList = 'header center';
        document.querySelector('.header').classList.add('header-form');
        document.querySelector('#nav_contacts').classList.add('active');
      }
      console.log(item.id, item.getBoundingClientRect().top)
    }
  })
}

// document.addEventListener('scroll', headerStyler)
// document.addEventListener('click', headerStyler)

function hideCookies(e){
  if (e.target.classList.contains('cookies-close')) {
    e.target.parentNode.style.animation = 'unset';
    if(window.innerWidth < 800) {
      e.target.parentNode.style.top = '110%'
    }
  }
}

document.addEventListener('click', hideCookies)

if(window.innerWidth < 600) {
	setTimeout(function(){
	  $('.img-parallax').css("display","block");
	}, 1800);
}