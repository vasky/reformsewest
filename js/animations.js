var controller = new ScrollMagic.Controller();
var progressBar = document.querySelector('.progress_bar');
var currentProgress = 0


$(function () { // wait for document ready

    // offer
      var scene = new ScrollMagic.Scene({
        triggerElement: ".offer",
        duration:700,
        offset:(window.innerHeight / 2),
      })
      .setPin(".offer")
      .addTo(controller);

      scene.on("progress", function (event) {
        if (event.progress >= 0 || event.progress < 1) {
          document.querySelector('.progress_bar').style.width = '0%';
          document.querySelector('.header').classList = 'header center';
        }
      });
    // offer

    // sceneEquipment
      var sceneEquipment = new ScrollMagic.Scene({
        triggerElement: ".equipment",
        duration:10000,
        offset:(window.innerHeight / 2),
      })
      .setPin(".equipment")
      .addTo(controller);
      sceneEquipment.setClassToggle('equipment_step-1', 'animated')
      sceneEquipment.on("progress", function (event) {
        if (event.progress >= 0 || event.progress < 1) {
          document.querySelectorAll('.src').forEach(function(item){
            item.classList.remove('current')
            if(item.id == 'nav_equipment'){
              item.classList.add('current');
            }
          })
          document.querySelector('.header').classList = 'header center';
          document.querySelector('.header').classList.add('header-equipment');
          document.querySelector('#nav_equipment').classList.add('active');
        }
        progressWatcher(progressBar, 0, event, 0)
        addAnimation('.header .btn', 0, event);
        addAnimation('.header .btn', 0.05, event);

        addAnimation('.equipment_step-1 .equipment_img', 0.000,event)
        addAnimation('.equipment_step-1 h2', 0.05,event)
        addAnimation('.equipment_step-1 p', 0.1,event)

        removeAnimation('.equipment_step-1 h2', 0.22,event)
        removeAnimation('.equipment_step-1 p', 0.22,event)
        addAnimation('.equipment_step-2 h2', 0.22,event)
        addAnimation('.equipment_step-2 p', 0.3,event)

        removeAnimation('.equipment_step-2 h2', 0.38,event)
        removeAnimation('.equipment_step-2 p', 0.38,event)
        addAnimation('.equipment_step-3 h2', 0.38,event)
        addAnimation('.equipment_step-3 p', 0.44,event)
        addAnimation('.equipment_step-3 .btn', 0.48,event)


        removeAnimation('.equipment_step-1 .equipment_img', 0.52,event)
        removeAnimation('.equipment_step-3 h2', 0.52,event)
        removeAnimation('.equipment_step-3 p', 0.52,event)
        addAnimation('.equipment_step-4 h2', 0.52,event)
        addAnimation('.equipment_step-4 p', 0.52,event)
        addAnimation('.equipment_step-4 .equipment_img', 0.52,event)

        removeAnimation('.equipment_step-4 .equipment_img', 0.74,event)
        removeAnimation('.equipment_step-4 h2', 0.74,event)
        removeAnimation('.equipment_step-4 p', 0.74,event)
        addAnimation('.equipment_step-5 h2', 0.82,event)
        addAnimation('.equipment_step-5 p', 0.9,event)
        addAnimation('.equipment_step-5 .equipment_img', 0.74,event)

        progressWatcher(progressBar, 0, event, 0)
        progressWatcher(progressBar, 0.16, event,3)
        progressWatcher(progressBar, 0.3, event,8)
        progressWatcher(progressBar, 0.48, event,13)
        progressWatcher(progressBar, .88, event,26)

      });
    // sceneEquipment

    // scenePyrolysis
      var scenePyrolysis = new ScrollMagic.Scene({
        triggerElement: ".pyrolysis",
        duration:10500,
        offset:(window.innerHeight / 2),
      })
      .setPin(".pyrolysis")
      .addTo(controller);

      scenePyrolysis.on("progress", function (event) {
        if (event.progress >= 0 || event.progress < 1) {
          document.querySelectorAll('.src').forEach(function(item){
            item.classList.remove('current')
            if(item.id == 'nav_technologies'){
              item.classList.add('current');
            }
          })
          document.querySelector('.header').classList = 'header center';
          document.querySelector('.header').classList.add('header-pyrolysis');
          document.querySelector('#nav_technologies').classList.add('active');
        }
        // if (event.progress >= 0) {
        //   document.querySelector('.header').classList.add('header-pyrolysis');
        //   document.querySelector('.header').classList.remove('header-equipment');
        //   document.querySelector('#nav_technologies').classList.add('active');

        // }
        // else {
        //   document.querySelector('.header').classList.remove('header-pyrolysis')
        //   document.querySelector('.header').classList.add('header-equipment');
        //   document.querySelector('#nav_technologies').classList.remove('active');

        // }
        progressWatcher(progressBar, 0.001, event, 25)
        progressWatcher(progressBar, 0.06, event, 25)
        progressWatcher(progressBar, 0.12, event,26)
        progressWatcher(progressBar, 0.18, event,26)
        progressWatcher(progressBar, 0.24, event,27)
        progressWatcher(progressBar, .36, event,28)
        progressWatcher(progressBar, 0.42, event, 29)
        progressWatcher(progressBar, 0.48, event,30)
        progressWatcher(progressBar, 0.56, event,32)
        progressWatcher(progressBar, 0.62, event,34)
        progressWatcher(progressBar, .68, event,36)
        progressWatcher(progressBar, 0.74, event, 37)
        progressWatcher(progressBar, 0.8, event,39)
        progressWatcher(progressBar, 0.86, event,41)
        progressWatcher(progressBar, .90, event,51)
        if(window.innerWidth < 800) {
          addAnimation('.pyrolysis_step-1', 0., event);
          removeAnimation('.pyrolysis_step-1', 0.06, event);
          addAnimation('.pyrolysis_step-2', 0.06, event);
          removeAnimation('.pyrolysis_step-2', 0.12, event);
          addAnimation('.pyrolysis_step-3', 0.12, event);
          removeAnimation('.pyrolysis_step-3', 0.18, event);
          addAnimation('.pyrolysis_step-4', 0.18, event);
          removeAnimation('.pyrolysis_step-4', 0.24, event);
          addAnimation('.pyrolysis_step-5', 0.24, event);
          removeAnimation('.pyrolysis_step-5', .36, event);
          addAnimation('.pyrolysis_step-6', 0.36, event);
          removeAnimation('.pyrolysis_step-6', 0.42, event);
          addAnimation('.pyrolysis_step-7', 0.42, event);
          removeAnimation('.pyrolysis_step-7', 0.48, event);
          addAnimation('.pyrolysis_step-8', 0.48, event);
          removeAnimation('.pyrolysis_step-8', 0.56, event);
          addAnimation('.pyrolysis_step-9', 0.56, event);
          removeAnimation('.pyrolysis_step-9', 0.62, event);
          addAnimation('.pyrolysis_step-10', 0.62, event);
          removeAnimation('.pyrolysis_step-10', .68, event);
          addAnimation('.pyrolysis_step-11', 0.68, event);
          removeAnimation('.pyrolysis_step-11', 0.74, event);
          addAnimation('.pyrolysis_step-12', 0.74, event);
          removeAnimation('.pyrolysis_step-12', 0.8, event);
          addAnimation('.pyrolysis_step-13', 0.8, event);
          removeAnimation('.pyrolysis_step-13', 0.86, event);
          addAnimation('.pyrolysis_step-14', 0.86, event);
          removeAnimation('.pyrolysis_step-14', 0.92, event);
          addAnimation('.pyrolysis_step-15', 0.92, event);
          removeAnimation('.pyrolysis_step-15', .99, event);
        }
        if(window.innerWidth > 800) {
          var item = document.querySelector('.pyrolysis_scene');
          if(event.progress >= 0.001){
            item.classList=('pyrolysis_scene scene-1')
          }
          if(event.progress >= 0.06) {
            item.classList=('pyrolysis_scene scene-2')
          }
          if(event.progress >= 0.12) {
            item.classList=('pyrolysis_scene scene-3')
          }
          if(event.progress >= 0.18) {
            item.classList=('pyrolysis_scene scene-4')
          }
          if(event.progress >= 0.24) {
            item.classList=('pyrolysis_scene scene-5')
          }
          if(event.progress >= 0.32) {
            item.classList=('pyrolysis_scene scene-6')
          }
          if(event.progress >= 0.4) {
            item.classList=('pyrolysis_scene scene-7')
          }
          if(event.progress >= 0.48) {
            item.classList=('pyrolysis_scene scene-8')
          }
          if(event.progress >= 0.56) {
            item.classList=('pyrolysis_scene scene-9')
          }
          if(event.progress >= 0.64) {
            item.classList=('pyrolysis_scene scene-10')
          }
          if(event.progress >= 0.72) {
            item.classList=('pyrolysis_scene scene-11')
          }
          if(event.progress >= 0.8) {
            item.classList=('pyrolysis_scene scene-12')
          }
          if(event.progress >= 0.88) {
            item.classList=('pyrolysis_scene scene-13')
          }
        }
      });
    // scenePyrolysis

    // sceneTechnologies

      var sceneTechnologies = new ScrollMagic.Scene({
        triggerElement: ".technologies",
        duration:4000,
        offset:(window.innerHeight / 2),
      })
      .setPin(".technologies")
      .addTo(controller);

      sceneTechnologies.on("progress", function (event) {
        progressWatcher(progressBar, 0.05, event, 43)

        // if (event.progress >= 0) {
        //   document.querySelector('.header').classList.add('header-technologies');
        //   document.querySelector('.header').classList.remove('header-pyrolysis');
        //   document.querySelector('#nav_pluses').classList.add('active');


        // }
        // else {
        //   document.querySelector('.header').classList.remove('header-technologies')
        //   document.querySelector('.header').classList.add('header-pyrolysis');
        //   document.querySelector('#nav_pluses').classList.remove('active');
        // }

        if (event.progress >= 0 || event.progress < 1) {
          document.querySelectorAll('.src').forEach(function(item){
            item.classList.remove('current')
            if(item.id == 'nav_technologies'){
              item.classList.add('current');
            }
          })
          document.querySelector('.header').classList = 'header center';
          // document.querySelector('.header').classList.add('header-plus');
          document.querySelector('#nav_pluses').classList.add('active');
        }


        addAnimation('.technologies_heading', 0, event);
        progressWatcher(progressBar, 0.1, event, 46)

        addAnimation('.technologies_desc1', 0.2, event);
        removeAnimation('.technologies_desc1', 0.6, event);
        progressWatcher(progressBar, 0.2, event,47)

        addAnimation('.technologies_desc2', 0.6, event);

        progressWatcher(progressBar, 0.6, event,48)

        addAnimation('.technologies_img', 0.4, event);
        removeAnimation('.technologies_img', 0.6, event);
        removeAnimation('.technologies_heading', 0.8, event);


        progressWatcher(progressBar, 0.8, event,49)
        if (event.progress > 0.8) {
          document.querySelector('.technologies_desc2').classList.add('second_animation');
        }
        else {
          document.querySelector('.technologies_desc2').classList.remove('second_animation');
        }
        addAnimation('.advantage_fuel', 0.8, event);

        if(event.progress > .9){
          document.querySelectorAll('.advantage_fuel p').forEach(function(item){
            item.classList.add('active')
          });
        }
        else {
          document.querySelectorAll('.advantage_fuel p').forEach(function(item){
            item.classList.remove('active')
          });
        }

        progressWatcher(progressBar, .9, event,49)

      });
    // sceneTechnologies

    // scenePlus
      var scenePlus = new ScrollMagic.Scene({
        triggerElement: ".plus",
        duration:10000,
        offset:(window.innerHeight / 2),
      })
      .setPin(".plus")
      .addTo(controller);

      scenePlus.on("progress", function (event) {
        // progressWatcher(progressBar, 0.05, event, 0)
        // toggleAnimation('.header .btn', 0.05, event);

        // if (event.progress >= 0) {
        //   document.querySelector('.header').classList.add('header-plus');
        //   document.querySelector('.header').classList.remove('header-technologies');

        // }
        // else {
        //   document.querySelector('.header').classList.remove('header-plus')
        //   document.querySelector('.header').classList.add('header-technologies');
        // }

        if (event.progress >= 0 || event.progress < 1) {
          document.querySelectorAll('.src').forEach(function(item){
            item.classList.remove('current')
            if(item.id == 'nav_pluses'){
              item.classList.add('current');
            }
          })
          document.querySelector('.header').classList = 'header center';
          document.querySelector('.header').classList.add('header-plus');
          document.querySelector('#nav_pluses').classList.add('active');
        }

        addAnimation('.plus_step1 .plus_heading', 0.000,event)
        removeAnimation('.plus_step1 .plus_heading', 0.045,event)

        addAnimation('.plus_step2 .waste_list', 0.045,event)
        addAnimation('.plus_step2 h2', 0.09,event)
        addAnimation('.plus_step2 .plus_step_content p', 0.135,event)

        removeAnimation('.plus_step2 .waste_list', 0.18,event)
        removeAnimation('.plus_step2 h2', 0.18,event)
        removeAnimation('.plus_step2 .plus_step_content p', 0.18,event)

        addAnimation('.plus_step3 .plus_step_img', 0.18,event)
        addAnimation('.plus_step3 h2', 0.225,event)
        addAnimation('.plus_step3 .plus_step_content p', .27,event)

        removeAnimation('.plus_step3 .plus_step_img', 0.315,event)

        addAnimation('.plus_step4 .plus_step_img', 0.315,event)
        removeAnimation('.plus_step4 .plus_step_img', 0.35,event)
        removeAnimation('.plus_step3 h2', 0.35,event)
        removeAnimation('.plus_step3 .plus_step_content p', .35,event)

        addAnimation('.plus_step5 .plus_step_img', 0.35,event)
        addAnimation('.plus_step5 h2', 0.395,event)
        addAnimation('.plus_step5 .plus_step_content p', .435,event)

        removeAnimation('.plus_step5 .plus_step_img', 0.48,event)
        removeAnimation('.plus_step5 h2', 0.48,event)
        removeAnimation('.plus_step5 .plus_step_content p', .48,event)

        addAnimation('.plus_step6 .plus_step_img', 0.48,event)
        addAnimation('.plus_step6 h2', 0.525,event)
        addAnimation('.plus_step6 .plus_step_content p', .57,event)

        removeAnimation('.plus_step6 .plus_step_img', 0.615,event)
        removeAnimation('.plus_step6 h2', 0.615,event)
        removeAnimation('.plus_step6 .plus_step_content p', .615,event)

        addAnimation('.plus_step7 .plus_step_img', 0.615,event)
        addAnimation('.plus_step7 h2', 0.66,event)
        addAnimation('.plus_step7 .plus_step_content p', .705,event)

        removeAnimation('.plus_step7 .plus_step_img', 0.795,event)
        removeAnimation('.plus_step7 h2', 0.795,event)
        removeAnimation('.plus_step7 .plus_step_content p', .795,event)

        addAnimation('.plus_step8 .plus_step_img', 0.795,event)
        addAnimation('.plus_step8 h2', 0.84,event)
        addAnimation('.plus_step8 .plus_step_content p', .885,event)

        removeAnimation('.plus_step8 .plus_step_img', 1,event)
        removeAnimation('.plus_step8 h2', 1,event)
        removeAnimation('.plus_step8 .plus_step_content p', 1,event)

        progressWatcher(progressBar, 0.00, event, 51)
        progressWatcher(progressBar, 0.25, event,53)
        progressWatcher(progressBar, 0.375, event,55)
        progressWatcher(progressBar, 0.5, event,57)
        progressWatcher(progressBar, 0.625, event,60)
        progressWatcher(progressBar, 0.75, event,62)
        progressWatcher(progressBar, 0.79, event,65)
        progressWatcher(progressBar, .8, event,77)

      });
    // scenePlus

    // sceneAbout
      var sceneAbout = new ScrollMagic.Scene({
        triggerElement: ".about",
        duration:4000,
        offset:(window.innerHeight / 2),
      })
      .setPin(".about")
      .addTo(controller);

      sceneAbout.on("progress", function (event) {
        progressWatcher(progressBar, 0.05, event, 72.5)
        // toggleAnimation('.header .btn', 0.05, event);

        // if (event.progress >= 0) {
        //   document.querySelector('.header').classList.add('header-about');
        //   document.querySelector('.header').classList.remove('header-plus');
        //   document.querySelector('#nav_about').classList.add('active');


        // }
        // else {
        //   document.querySelector('.header').classList.remove('header-about')
        //   document.querySelector('.header').classList.add('header-plus');
        //   document.querySelector('#nav_about').classList.remove('active');

        // }

        addAnimation('.aboutFirstTitle', 0.00, event);
        removeAnimation('.aboutFirstTitle', 0.25, event);
        progressWatcher(progressBar, 0.01, event, 77)

        addAnimation('.about_text-1', 0.25, event);
        progressWatcher(progressBar, 0.3, event,80)

        addAnimation('.about_text-2', 0.5, event);
        progressWatcher(progressBar, 0.5, event,90)

        addAnimation('.about_text-3', 0.75, event);
        progressWatcher(progressBar, 0.7, event,95)
        progressWatcher(progressBar, 0.9, event,100)
        if (event.progress >= 0 || event.progress < 1) {
          document.querySelectorAll('.src').forEach(function(item){
            item.classList.remove('current')
            if(item.id == 'nav_about'){
              item.classList.add('current');
            }
          })
          document.querySelector('.header').classList = 'header center';
          document.querySelector('.header').classList.add('header-about');
          document.querySelector('#nav_about').classList.add('active');
        }

      });
    // sceneAbout

    // sceneContactUs
      var sceneContactUs = new ScrollMagic.Scene({
        triggerElement: ".contact_us",
        duration:1000,
        offset:(0),
      })
      .setClassToggle('.header', 'header-form')
      .setClassToggle('.contact_us', 'animated')

      .addTo(controller);
      sceneContactUs.on("progress", function (event) {
        if (event.progress >= 0 || event.progress <= 1) {
          document.querySelectorAll('.src').forEach(function(item){
            item.classList.remove('current')
            if(item.id == 'nav_contacts'){
              item.classList.add('current');
            }
          })
          document.querySelector('.header').classList = 'header center';

          document.querySelector('#nav_contacts').classList.add('active');
        }
        if(event.progress > 0.5) {
          document.querySelector('.header').classList.add('header-form');
        }
      })
  });

function toggleBlocks(elem, progressStart, progressEnds,event) {
  if(event.progress >= progressStart && event.progress < progressEnds){
    document.querySelectorAll(elem).forEach(function(item) {
      item.classList.add('animated')
    })
  }
  else {
    document.querySelectorAll(elem).forEach(function(item) {
      item.classList.remove('animated')
    })
  };
}

function addAnimation(elem, progress,event) {
  if(event.progress >= progress){
      document.querySelector(elem).classList.add('animated')
  }
  else {
    document.querySelector(elem).classList.remove('animated')
  };
}

function removeAnimation(elem, progress,event) {
  if(event.progress >= progress){
    document.querySelector(elem).classList.add('animation_ends')
  }
  else {
    document.querySelector(elem).classList.remove('animation_ends')
  };
}

function progressWatcher(elem, progress,event,currentProgress) {
  if(event === 'clickNav'){
    elem.style.width = currentProgress + "%"
  }
  if(event.progress > progress){
    elem.style.width = currentProgress + "%"
  }
}


// fix 02 12 2020
document.addEventListener("DOMContentLoaded", function(event){
  const navLinks = document.querySelector("#menuNav").querySelectorAll("a");
  navLinks.forEach(a => {
      correctCalcProgress(a, navLinks);

  })
});

// pageYOffset
// $(item.getAttribute('href'))

function correctCalcProgress(item, navLinks) {
  const textLink = item.textContent.trim();
  let intervalPoint;
  item.addEventListener("click", ()=>{
    intervalPoint = showCurrentPoints( navLinks, item )
    switch (textLink){
      case "Оборудование":
        progressWatcher(progressBar, 0,'clickNav',0)
        currentScroll(item, 2808, 700)
        break;
      case "Технология":
        progressWatcher(progressBar, 0.5, 'clickNav',26)
        currentScroll(item, 12611, 100)
        break;
      case "Преимущества":
        progressWatcher(progressBar, 0.5, 'clickNav',51)
        currentScroll(item, 29005, 700)
        break;
      case "О компании":
        progressWatcher(progressBar, 0.5, 'clickNav',77)
        currentScroll(item, 39941, 500)
        break;
      case "Контакты":
        progressWatcher(progressBar, 0.5, 'clickNav',100)
        currentScroll(item, 44839, 300)
        break;
    }
  })
}

function currentScroll(item, dn, scrool) {
  // dn = $(item.getAttribute('href')).offset().top
  const olddn = dn
  $(window)[0].pageYOffset > dn ? dn += scrool : dn -= scrool
  $('html, body').animate({scrollTop: dn}, 1);
  $('html, body').animate({scrollTop: olddn}, 1000);

}
function showCurrentPoints(navLinks, item) {
  const intervalPoint = setTimeout(()=>{
    navLinks = Array.prototype.slice.call(navLinks)
    const activeLink = navLinks.indexOf(item)
    navLinks.forEach( a => {
      const indexA = navLinks.indexOf(a);
      if( indexA >= activeLink + 1) {
        navLinks[indexA].classList = "src"
      }
    })
  }, 200)

  return intervalPoint;
}